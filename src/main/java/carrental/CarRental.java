package carrental;

import java.util.Arrays;

public class CarRental {
	
	String[] contracts;
	int counter = 0;
	
	public CarRental() {
		
		this.contracts = new String[2];
	}
	
	public void addContract(CarRentalContract c1) {
		
		this.contracts[counter] = c1.toString();
		counter =+ 1;
	}
	
	public String displayContracts() {
		String str = "";
		
		for (int i = 0; i < contracts.length; i++) {
			str +=  contracts[i].toString() + "\n";
		} 
		
		return str;
	}
	
	public String toString(String contracts) {
		
		return contracts.toString();
	}

}
