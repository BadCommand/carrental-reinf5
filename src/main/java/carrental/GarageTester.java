/*
 * Project and Training 1 - HS19/20 (Java), Computer Science, Berner Fachhochschule
 */

package carrental;

public class GarageTester {
	
	public static Garage getExample() {
		// Your code 
		Truck truck = new Truck("black", false);
		
		Garage garage = new Garage();
		garage.setVehicle(truck);
		 
		return garage;
	}
	
	public static void main(Garage truck) {
		Garage g = getExample(); 
	}

}
