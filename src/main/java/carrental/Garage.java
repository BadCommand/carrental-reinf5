package carrental;

public class Garage {
	
	public Vehicle vehicle;

	public Garage(){
	}
	
	public void setVehicle(Vehicle parked) {
		
		this.vehicle = parked;
	}
	
	public String toString() {
		
		return "Description of the parked vehicle: " + this.vehicle;
	}
}
