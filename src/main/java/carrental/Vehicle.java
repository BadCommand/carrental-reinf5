package carrental;

public class Vehicle {
	
	String vehicleColor = "default";
	
	public Vehicle(String color) {
		this.vehicleColor = color;
	}
	
	public String getColor() {
		
		return this.vehicleColor;
	}
	
	public String toString() {
		
		return "This vehicle is " + vehicleColor;
	}

}
