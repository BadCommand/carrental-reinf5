package carrental;

public class CarRentalContract {

	private Customer costumer;
	private Vehicle vehicle;

	public CarRentalContract(Customer costumer, Vehicle vehicle) {
		this.costumer = costumer;
		this.vehicle = vehicle;
	}
	
	public String toString() {
		
		return costumer.name + ":" + vehicle.toString();
	}
}
