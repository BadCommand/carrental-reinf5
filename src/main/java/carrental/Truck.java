package carrental;

public class Truck extends Vehicle {

	private boolean hasTrailer = false;
	
	public Truck(String color, boolean hasTrailer) {
		super(color);
		this.hasTrailer = hasTrailer;
	}
	
	public Truck(String color) {
		super(color);
	}

	@Override
	public String toString() {

		if (hasTrailer == true) {
			return super.toString()+ ", has Trailer: true";
		} else {
			return super.toString() + ", has Trailer: false";
		}
	}
}
