package carrental;

public class Car extends Vehicle {
	
	private boolean hasWinterTires = false;

	public Car(String color, boolean winterTires) {
		super(color);
		this.hasWinterTires = winterTires;
	}
	
	public Car(String color) {
		super(color);
	}

	@Override
	public String toString() {

		if (hasWinterTires == true) {
			return super.toString() + ", has WinterTires: true";
		} else {
			return super.toString() + ", has WinterTires: false";
		}
	}

}
